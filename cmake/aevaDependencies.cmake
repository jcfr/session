##
## Python interpreter and pybind11
##
if (AEVA_ENABLE_PYTHON)
  # Only look for python packages if they are not already found (this is a
  # workaround for pybind11's custom python cmake scripts)
  find_package(Python3 REQUIRED COMPONENTS Interpreter Development)

  # Set the python module extension (needed for pybind11)
  if (DEFINED Python3_SOABI)
    set(PYTHON_MODULE_EXTENSION "${Python3_SOABI}")
  elseif(WIN32)
    set(PYTHON_MODULE_EXTENSION ".pyd")
  else()
    set(PYTHON_MODULE_EXTENSION ".so")
  endif()
  set(PYTHON_MODULE_PREFIX "")

  find_package(pybind11 2.3 REQUIRED)
  set(pybind11_VERSION "2.3")

  # Pybind11 settings
  set(PYBIND11_FLAGS " ")
  if(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
      CMAKE_CXX_COMPILER_ID MATCHES "GNU" OR
      CMAKE_CXX_COMPILER_ID MATCHES "Intel")
    set(PYBIND11_FLAGS "${PYBIND11_FLAGS} -Wno-shadow")
  endif()
endif ()

##
## SMTK
##
find_package(smtk
  REQUIRED
)
if (AEVA_ENABLE_PYTHON AND NOT SMTK_ENABLE_PYTHON_WRAPPING)
  message(FATAL_ERROR
    "Python support in the AEVA session requires Python support from SMTK.")
endif ()

##
## VTK
##
find_package(VTK
  COMPONENTS
    CommonCore
    CommonDataModel
    IOXML
  REQUIRED
)

##
## ITK and ITKVTKGlue
##
find_package(ITK
  COMPONENTS
    ITKCommon
    ITKIOImageBase
    ITKIONRRD
    ITKIONIFTI
    ITKVtkGlue
  REQUIRED
)
include(${ITK_USE_FILE})

##
## Netgen
##
find_package(Netgen
  QUIET
  REQUIRED
)
