#ifndef vtkVisitation_h
#define vtkVisitation_h

/// Should visitation stop or continue?
enum class vtkVisitation
{
  CONTINUE, //!< Do not terminate early, continue invoking the functor.
  STOP      //!< Terminate early; avoid invoking the functor again.
};

/// What rule should be used to determine visited data?
enum class vtkVisitationRule
{
  CELLS_CONNECTED_TO_POINTS, //!< Visit all cells whose connectivity contains one or more given point IDs.
  CELLS_COMPOSED_OF_POINTS, //!< Visit cells whose connectivity *exclusively contains* point IDs from the given set.
  POINTS_CONNECTED_TO_CELLS, //!< Visit all points in the connectivity of onee or more given cell IDs.
  POINTS_INTERNAL_TO_CELLS   //!< Visit points that bound only cells with IDs in the given set.
};

#endif // vtkVisitation_h
