#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

set (unit_tests
)

set (unit_tests_which_require_data
  TestBooleans.cxx
  TestEditFreeformAttributes.cxx
  TestExpectedArrays.cxx
  TestFeatureSelectionOps.cxx
  TestGrowSelection.cxx
  TestImprintGeometryOp.cxx
  TestImageImportExport.cxx
  TestMedImportExport.cxx
  TestProximitySelection.cxx
  TestReconstructSurfaceOp.cxx
  TestResourceReadWrite.cxx
  TestSmoothSurfaceOp.cxx
  TestStlImportMedExport.cxx
  UnitTestImport.cxx
)

set(external_libs
  ${ITK_LIBRARIES}
  VTK::CommonDataModel
  VTK::CommonTransforms
  VTK::FiltersCore
)

unit_tests(
  LABEL "AEVASession"
  SOURCES ${unit_tests}
  SOURCES_REQUIRE_DATA ${unit_tests_which_require_data}
  LIBRARIES smtkCore smtkAEVASession ${external_libs}
)
