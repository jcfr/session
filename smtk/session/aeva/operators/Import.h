//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_Import_h
#define smtk_session_aeva_Import_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

#include "vtkImageData.h"
#include "vtkSmartPointer.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class SMTKAEVASESSION_EXPORT Import : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::Import);
  smtkCreateMacro(Import);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

  static vtkSmartPointer<vtkImageData> readVTKImage(const std::string& filename);

protected:
  Result operateInternal() override;
  Specification createSpecification() override;
  virtual const char* xmlDescription() const override;

  virtual Result importVTKImage(const smtk::session::aeva::Resource::Ptr&);
  virtual Result importITKImage(const smtk::session::aeva::Resource::Ptr&);
  virtual Result importMedMesh(const smtk::session::aeva::Resource::Ptr&);
  virtual Result importExodusMesh(const smtk::session::aeva::Resource::Ptr&);
  virtual Result importVTKMesh(const smtk::session::aeva::Resource::Ptr&);

  static std::set<std::string> supportedITKFileFormats();
  static std::map<std::string, std::set<std::string> > supportedVTKFileFormats();

  Result m_result;
  std::vector<smtk::common::UUID> m_preservedUUIDs;
};

SMTKAEVASESSION_EXPORT smtk::resource::ResourcePtr importResource(const std::string&);
}
}
}

#endif
