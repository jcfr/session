//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_aeva_ExportFeb_h
#define __smtk_session_aeva_ExportFeb_h

#include "smtk/session/aeva/Operation.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class SMTKAEVASESSION_EXPORT ExportFeb : public Operation
{
public:
  smtkTypeMacro(smtk::session::aeva::ExportFeb);
  smtkCreateMacro(ExportFeb);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  Result operateInternal() override;

  virtual const char* xmlDescription() const override;
  Result m_result;
};

}
}
}

#endif
