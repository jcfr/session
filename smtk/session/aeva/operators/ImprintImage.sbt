<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA session "imprint image" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="imprint image" Label="Imprint Image" BaseType="operation">

      <BriefDescription>Imprint nodal attributes from an image onto a mesh.</BriefDescription>
      <DetailedDescription>
        Imprint the scalar values from a vtkImageData onto points of the target poly data/unstructured grid.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input poly data/unstructured grid. Point field data named "ImageScalars" will be added.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face|volume"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Component Name="image" NumberOfRequiredValues="1">
          <BriefDescription>The source image to be probed for scalars.</BriefDescription>
          <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="volume"/></Accepts>
        </Component>
      </ItemDefinitions>

    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(imprint image)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
