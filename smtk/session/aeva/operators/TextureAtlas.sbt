<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA session "texture atlas" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="texture atlas" Label="Texture Atlas" BaseType="operation">
      <BriefDescription>Create a texture altas from an input surface mesh with chart ids (cell data).</BriefDescription>
      <DetailedDescription>
        Create a texture altas from an input surface mesh with chart ids assigned to its cell data by following the
        LSCM paper.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input surface to mesh</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="texel size" Label="Texel Size">
          <BriefDescription>Size of texel for shape discretization</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>

        <Double Name="texture atlas width" Label="Texture Altas Width">
          <BriefDescription>Width of texture atlas in parameterization space</BriefDescription>
          <DefaultValue>1000.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>

        <Double Name="boundary spacing" Label="Boundary Spacing">
          <BriefDescription>Space on the boundary between partitions during packing</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(textrue altas)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
