set(data
  1.3.12.2.1107.5.2.19.45406.2014120211045428131243076.0.0.0_small.nii
)

# Install sample data for use by the aeva application
smtk_get_kit_name(name dir_prefix)
install(
  FILES
    ${data}
  DESTINATION
    share/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix}
)
