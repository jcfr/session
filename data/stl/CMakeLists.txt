set(data
  box.stl
)

# Install sample data for use by the aeva application
smtk_get_kit_name(name dir_prefix)
install(
  FILES
    ${data}
  DESTINATION
    share/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix}
)
